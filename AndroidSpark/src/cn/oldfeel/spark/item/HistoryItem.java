package cn.oldfeel.spark.item;

import android.database.Cursor;

public class HistoryItem {
	public String from;
	public String body;
	public String time;
	public int count; // 未读信息条数

	public HistoryItem(Cursor cursor) {
		from = cursor.getString(cursor.getColumnIndex("msg_from"));
		body = cursor.getString(cursor.getColumnIndex("msg_body"));
		time = cursor.getString(cursor.getColumnIndex("msg_time"));
		count = cursor.getInt(cursor.getColumnIndex("count"));
	}
}
