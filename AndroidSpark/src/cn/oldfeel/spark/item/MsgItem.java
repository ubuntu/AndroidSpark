package cn.oldfeel.spark.item;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * 消息
 * 
 * @author oldfeel
 * @create Date:2013年11月8日上午9:29:06
 */
public class MsgItem {
	String userid;
	String msg;
	String date;
	String from;
	String type = TYPE[2];// 类型 normal 普通消息
	String receive;// 接收
	String time;// 语音时长
	String filePath;

	public static final String USERID = "userid";
	public static final String MSG_CONTENT = "msg";// 消息内容
	public static final String DATE = "date";
	public static final String FROM = "from";
	public static final String MSG_TYPE = "type";
	public static final String RECEIVE_STAUTS = "receive";// 接收状态
	public static final String TIME_REDIO = "time";
	public static final String FIL_PAHT = "filePath";

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getFileName() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public static final String[] STATUS = { "success", "refused", "fail",
			"wait" };
	public static final String[] TYPE = { "record", "photo", "normal" };
	public static final String[] FROM_TYPE = { "IN", "OUT" };

	public MsgItem() {

	}

	public MsgItem(String userid, String msg, String date, String from) {
		this.userid = userid;
		this.msg = msg;
		this.date = date;
		this.from = from;
	}

	public MsgItem(String userid, String msg, String date, String from,
			String type, String receive, String time, String filePath) {
		super();
		this.userid = userid;
		this.msg = msg;
		this.date = date;
		this.from = from;
		this.type = type;
		this.receive = receive;
		this.time = time;
		this.filePath = filePath;
	}

	public String getType() {
		return type;
	}

	@Override
	public String toString() {
		return "Msg [userid=" + userid + ", msg=" + msg + ", date=" + date
				+ ", from=" + from + ", type=" + type + ", receive=" + receive
				+ ", time=" + time + ", filePath=" + filePath + "]";
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getReceive() {
		return receive;
	}

	public void setReceive(String receive) {
		this.receive = receive;
	}

	public static String[] getStatus() {
		return STATUS;
	}

	public MsgItem(String userid, String msg, String date, String from,
			String type, String receive) {
		super();
		this.userid = userid;
		this.msg = msg;
		this.date = date;
		this.from = from;
		this.type = type;
		this.receive = receive;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * 分析消息内容
	 * 
	 * @param body
	 *            Json
	 */
	public static MsgItem analyseMsgBody(String jsonStr) {
		MsgItem msg = new MsgItem();
		// 获取用户、消息、时间、IN
		try {
			JSONObject jsonObject = new JSONObject(jsonStr);
			msg.setUserid(jsonObject.getString(MsgItem.USERID));
			msg.setFrom(jsonObject.getString(MsgItem.FROM));
			msg.setMsg(jsonObject.getString(MsgItem.MSG_CONTENT));
			msg.setDate(jsonObject.getString(MsgItem.DATE));
			msg.setType(jsonObject.getString(MsgItem.MSG_TYPE));
			msg.setReceive(jsonObject.getString(MsgItem.RECEIVE_STAUTS));
			msg.setTime(jsonObject.getString(MsgItem.TIME_REDIO));
			msg.setFilePath(jsonObject.getString(MsgItem.FIL_PAHT));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return msg;
	}

	/**
	 * 传json
	 */
	public static String toJson(MsgItem msg) {
		JSONObject jsonObject = new JSONObject();
		String jsonStr = "";
		try {
			jsonObject.put(MsgItem.USERID, msg.getUserid() + "");
			jsonObject.put(MsgItem.MSG_CONTENT, msg.getMsg() + "");
			jsonObject.put(MsgItem.DATE, msg.getDate() + "");
			jsonObject.put(MsgItem.FROM, msg.getFrom() + "");
			jsonObject.put(MsgItem.MSG_TYPE, msg.getType() + "");
			jsonObject.put(MsgItem.RECEIVE_STAUTS, msg.getReceive() + "");
			jsonObject.put(MsgItem.TIME_REDIO, msg.getTime());
			jsonObject.put(MsgItem.FIL_PAHT, msg.getFileName());
			jsonStr = jsonObject.toString();
			Log.d("msg json", jsonStr + "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonStr;
	}

}