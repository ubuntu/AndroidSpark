package cn.oldfeel.spark.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * 处理edittext的帮助类
 * 
 * @author oldfeel
 * 
 */
public class ETUtil {
	/**
	 * 获取edittext中内容
	 * 
	 * @param et
	 * @return
	 */
	public static String getText(EditText et) {
		return et.getText().toString().trim();
	}

	/**
	 * 判断edittext是否为空
	 * 
	 * @param editTexts
	 * @return
	 */
	public static boolean isEmpty(EditText... editTexts) {
		for (EditText editText : editTexts) {
			String content = editText.getText().toString().trim();
			if (content == null || content.length() == 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 检查两个输入框内容是否一样
	 * 
	 * @param etNewPassword
	 * @param etNewPassword2
	 * @return
	 */
	public static boolean isEqual(EditText etNewPassword,
			EditText etNewPassword2) {
		if (getText(etNewPassword2).equals(getText(etNewPassword))) {
			return true;
		}
		return false;
	}

	/**
	 * 将edittext回复为空
	 * 
	 * @param editTexts
	 */
	public static void setEmpty(EditText... editTexts) {
		for (EditText editText : editTexts) {
			editText.setText("");
		}
	}

	/**
	 * 判断是否为手机号码
	 * 
	 * @param mobiles
	 * @return
	 */
	public static boolean isMobileNO(EditText mobiles) {
		Pattern p = Pattern
				.compile("^((13[0-9])|(14[5,7])|(15[^4,\\D])|(18[0-9]))\\d{8}$");
		Matcher m = p.matcher(getText(mobiles));
		return m.matches();
	}

	/**
	 * 隐藏软键盘
	 * 
	 * @param editText
	 */
	public static void hideSoftKeyboard(Context context, EditText editText) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
	}
}
