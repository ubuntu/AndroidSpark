package cn.oldfeel.spark.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;

/**
 * 该类中的toast会覆盖上一个toast
 */
public class DialogUtil {
	private static DialogUtil tu;

	public static DialogUtil getInstance() {
		if (tu == null) {
			tu = new DialogUtil();
		}
		return tu;
	}

	private static Toast toast;

	public void showToast(Context context, String text) {
		if (toast != null) {
			toast.cancel();
		}
		toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
		toast.show();
	}

	private ProgressDialog pd;

	public void showPd(Context context, String message) {
		Log.d("example", "start request");
		if (pd != null) {
			pd.cancel();
		}
		pd = new ProgressDialog(context);
		pd.setMessage(message);
		pd.setIndeterminate(true);
		pd.setCancelable(true);
		pd.show();
	}

	public void cancelPd() {
		Log.d("example", "finish request");
		if (pd != null) {
			pd.cancel();
		}
	}

	/**
	 * 显示暂无数据的dialog
	 * 
	 * @param activity
	 */
	public void showEmpty(final Activity activity) {
		new AlertDialog.Builder(activity)
				.setTitle("抱歉,这里暂时没有数据...\n返回上一页面?")
				.setPositiveButton(android.R.string.ok,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								activity.finish();
							}
						}).show();
	}

	/**
	 * 显示加载完成所有数据的toast
	 * 
	 * @param context
	 */
	public void showLoadComplate(Context context) {
		showToast(context, "您已经加载完成所有数据");
	}

	/**
	 * 显示dialogfragment
	 * 
	 * @param fm
	 * @param fragment
	 */
	public void showDialog(FragmentManager fm, DialogFragment fragment) {
		FragmentTransaction ft = fm.beginTransaction();
		Fragment prev = fm.findFragmentByTag("dialog");
		if (prev != null) {
			ft.remove(prev);
		}
		ft.addToBackStack(null);
		fragment.show(ft, "dialog");
	}
}
