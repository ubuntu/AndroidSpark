package cn.oldfeel.spark.util;

import java.io.IOException;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

/**
 * 媒体工具�?
 * 
 * @author oldfeel
 * @create Date:2013�?0�?7日下�?:14:24
 */
public class MediaUtil {

	public static void ring(Activity activity, int rawId) {
		MediaPlayer mediaPlayer = MediaPlayer.create(activity, rawId);
		mediaPlayer.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				mp.release();
			}
		});
		mediaPlayer.start();
	}

	public static void ring(String path) {
		MediaPlayer mp = new MediaPlayer();
		try {
			mp.setDataSource(path);
			mp.prepare();
			mp.start();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		mp.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				mp.release();
			}
		});
	}

}
