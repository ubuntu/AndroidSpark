package cn.oldfeel.spark.util;

import android.util.Log;

/**
 * 打印日志
 * 
 * @author oldfeel
 * 
 */
public class LogUtil {
	/** true 为显示log信息，false为不显示 */
	public static boolean isDebug = true;

	/**
	 * 打印tag为example的log日志
	 * 
	 * @param log
	 */
	public static void showLog(Object log) {
		if (isDebug)
			Log.d("example", log.toString());
	}
}
