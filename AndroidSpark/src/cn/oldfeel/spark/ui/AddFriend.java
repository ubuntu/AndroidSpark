package cn.oldfeel.spark.ui;

import android.annotation.SuppressLint;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import cn.oldfeel.spark.R;
import cn.oldfeel.spark.base.BaseActivity;
import cn.oldfeel.spark.util.ETUtil;
import cn.oldfeel.spark.util.XMPPHelper;

/**
 * 添加好友
 * 
 * @author oldfeel
 * 
 */
@SuppressLint("HandlerLeak")
public class AddFriend extends BaseActivity implements TabListener,
		OnClickListener {
	EditText etContent;
	Button btnSubmit;
	/** true用户名查找添加好友 ,false为电话号码查找添加好友 */
	private boolean isName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_friend);
		initView();
		initListener();
		super.addTab(this, "用户名", "手机号");
	}

	private void initView() {
		etContent = (EditText) findViewById(R.id.add_friend_content);
		btnSubmit = (Button) findViewById(R.id.add_friend_submit);
	}

	private void initListener() {
		btnSubmit.setOnClickListener(this);
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		if (tab.getText().equals("用户名")) {
			isName = true;
			etContent.setHint(R.string.input_name);
		} else {
			isName = false;
			etContent.setHint(R.string.input_phone);
			super.showToast("暂不支持手机号码查找");
		}
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onClick(View v) {
		submit();
	}

	/**
	 * 提交添加好友
	 */
	private void submit() {
		if (isName) {
			XMPPHelper.getInstance().addFriend(handler,
					ETUtil.getText(etContent));
		}
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case XMPPHelper.ADD_FAIL:
				showToast("添加失败");
				break;
			case XMPPHelper.ADD_SUCCESS:
				showToast("添加成功");
				etContent.setText("");
				break;
			default:
				break;
			}
		};
	};

}
