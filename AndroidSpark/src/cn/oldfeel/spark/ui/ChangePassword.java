package cn.oldfeel.spark.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import cn.oldfeel.spark.R;
import cn.oldfeel.spark.base.BaseActivity;
import cn.oldfeel.spark.util.ETUtil;
import cn.oldfeel.spark.util.XMPPHelper;

/**
 * 修改密码
 * 
 * @author oldfeel
 * 
 */
@SuppressLint("HandlerLeak")
public class ChangePassword extends BaseActivity implements OnClickListener {
	private EditText etOldPassword, etNewPassword, etNewPassword2;
	private Button btnSubmit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_password);
		initView();
		initListener();
	}

	private void initView() {
		etOldPassword = (EditText) findViewById(R.id.change_password_old);
		etNewPassword = (EditText) findViewById(R.id.change_password_new);
		etNewPassword2 = (EditText) findViewById(R.id.change_password_new_2);
		btnSubmit = (Button) findViewById(R.id.change_password_submit);
	}

	private void initListener() {
		btnSubmit.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.change_password_submit:
			submit();
			break;

		default:
			break;
		}
	}

	/**
	 * 提交修改密码
	 */
	private void submit() {
		if (ETUtil.isEmpty(etNewPassword, etNewPassword2, etOldPassword)) {
			showToast("请填写完整信息 ");
			return;
		}
		if (!ETUtil.isEqual(etNewPassword, etNewPassword2)) {
			showToast("输入的两次密码不匹配");
		} else {
			XMPPHelper.changePassword(handler, ETUtil.getText(etNewPassword));
		}
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case XMPPHelper.CHANGE_PASSWORD_FAIL:
				showToast("修改失败");
				break;
			case XMPPHelper.CHANGE_PASSWORD_SUCCESS:
				showToast("修改成功");
				break;

			default:
				break;
			}
		};
	};
}
