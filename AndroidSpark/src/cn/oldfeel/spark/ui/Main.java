package cn.oldfeel.spark.ui;

import java.util.ArrayList;

import org.jivesoftware.smack.SmackAndroid;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import cn.oldfeel.spark.R;
import cn.oldfeel.spark.base.AppManager;
import cn.oldfeel.spark.config.UserInfo;
import cn.oldfeel.spark.db.ChatDbHelper;
import cn.oldfeel.spark.fragment.HistoryList;
import cn.oldfeel.spark.util.DialogUtil;
import cn.oldfeel.spark.util.XMPPHelper;

@SuppressLint("HandlerLeak")
public class Main extends ActionBarActivity {
	private DrawerLayout drawer;
	private ActionBarDrawerToggle toggle;
	private FrameLayout rightDrawer;
	private MainMenuFragment menuFragment;
	private MainFriendFragment friendFragment;
	private HistoryList historyList;
	private ArrayList<Fragment> list = new ArrayList<Fragment>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		AppManager.getInstance().addActivity(this);
		SmackAndroid.init(this);
		XMPPHelper.getInstance().connect(handler, UserInfo.getInstance(this));

		drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		rightDrawer = (FrameLayout) findViewById(R.id.right_drawer);
		toggle = new ActionBarDrawerToggle(this, drawer, R.drawable.ic_drawer,
				R.string.app_name, R.string.app_name);
		drawer.setDrawerListener(toggle);

		initFragment();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (toggle.onOptionsItemSelected(item)) {
			return true;
		}
		switch (item.getItemId()) {
		case R.id.main_action_friend:
			if (drawer.isDrawerOpen(rightDrawer)) {
				closeDrawers();
			} else {
				drawer.openDrawer(rightDrawer);
				friendFragment.onActivityCreated(null);
			}
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		toggle.syncState();
	}

	private void initFragment() {
		historyList = HistoryList.newInstance();
		menuFragment = MainMenuFragment.newInstance();
		friendFragment = MainFriendFragment.newInstance();
		FragmentTransaction trans = getSupportFragmentManager()
				.beginTransaction();
		trans.replace(R.id.content_frame, historyList);
		trans.replace(R.id.left_drawer, menuFragment);
		trans.replace(R.id.right_drawer, friendFragment);
		trans.commit();
	}

	/**
	 * 显示指定fragment
	 * 
	 * @param id
	 * @param fragment
	 */
	protected void showFragment(Fragment fragment) {
		closeDrawers();
		FragmentTransaction trans = getSupportFragmentManager()
				.beginTransaction();
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).isAdded()) {
				trans.hide(list.get(i));
			}
		}
		if (fragment.isAdded()) {
			trans.show(fragment);
		} else {
			trans.add(R.id.content_frame, fragment);
			list.add(fragment);
		}
		trans.commit();
	}

	/**
	 * 监听xmpp连接的handler
	 */
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case XMPPHelper.CONNECT_SUCCESS:
				DialogUtil.getInstance().showToast(Main.this, "连接成功");
				break;
			case XMPPHelper.CONNECT_FAIL:
				DialogUtil.getInstance().showToast(Main.this, "连接失败 ");
				break;
			case XMPPHelper.LOGIN_SUCCESS:// 自动登录成功
				menuFragment.setUserName(UserInfo.getInstance(Main.this));
				friendFragment.updateFriendList();
				break;
			default:
				break;
			}
		};
	};

	/**
	 * 开始聊天
	 * 
	 * @param name
	 */
	public void startChat(String name) {
		closeDrawers();
		Intent intent = new Intent(Main.this, ChatActivity.class);
		intent.putExtra("name", name);
		startActivity(intent);
	}

	public void onBackPressed() {
		XMPPHelper.getInstance().disConnect();
		ChatDbHelper.getInstance(Main.this).close();
		super.onBackPressed();
	}

	public void closeDrawers() {
		drawer.closeDrawers();
	}
}
