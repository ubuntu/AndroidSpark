package cn.oldfeel.spark.ui;

import android.annotation.SuppressLint;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import cn.oldfeel.spark.R;
import cn.oldfeel.spark.base.BaseActivity;
import cn.oldfeel.spark.config.UserInfo;
import cn.oldfeel.spark.util.DialogUtil;
import cn.oldfeel.spark.util.ETUtil;
import cn.oldfeel.spark.util.XMPPHelper;

/**
 * 登录
 * 
 * @author oldfeel
 * 
 */
@SuppressLint("HandlerLeak")
public class Login extends BaseActivity implements OnClickListener, TabListener {
	private EditText etAccount, etPassword, etPassword2;
	private Button btnSubmit;
	/** true为登录,false为注册 */
	private boolean isLogin = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		initView();
		initListener();
		super.addTab(this, "登录", "注册");
	}

	/**
	 * 初始化视图
	 */
	private void initView() {
		etAccount = (EditText) findViewById(R.id.login_account);
		etPassword = (EditText) findViewById(R.id.login_password);
		etPassword2 = (EditText) findViewById(R.id.login_password_2);
		btnSubmit = (Button) findViewById(R.id.login_submit);
	}

	/**
	 * 初始化监听
	 */
	private void initListener() {
		btnSubmit.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.login_submit:
			submit();
			break;

		default:
			break;
		}
	}

	/**
	 * 提交注册/登录
	 */
	private void submit() {
		if (isLogin) { // 登录
			login(ETUtil.getText(etAccount), ETUtil.getText(etPassword));
		} else { // 注册
			if (!ETUtil.isEqual(etPassword, etPassword2)) {
				DialogUtil.getInstance().showToast(this, "输入的两次密码不匹配");
			} else {
				regist(ETUtil.getText(etAccount), ETUtil.getText(etPassword));
			}
		}
	}

	/**
	 * 注册
	 * 
	 * @param account
	 * @param password
	 */
	private void regist(String account, String password) {
		if (ETUtil.isEmpty(etAccount, etPassword, etPassword2)) {
			DialogUtil.getInstance().showToast(this, "请填写完整信息");
			return;
		}
		XMPPHelper.getInstance().regist(handler, account, password);
	}

	/**
	 * 登录
	 * 
	 * @param account
	 * @param password
	 */
	private void login(String account, String password) {
		if (ETUtil.isEmpty(etAccount, etPassword)) {
			DialogUtil.getInstance().showToast(this, "请填写完整信息");
			return;
		}
		XMPPHelper.getInstance().login(handler, account, password);
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		if (tab.getText().equals("注册")) { // 注册
			etPassword2.setVisibility(View.VISIBLE);
			isLogin = false;
		} else { // 登录
			etPassword2.setVisibility(View.GONE);
			isLogin = true;
		}
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case XMPPHelper.CONNECT_FAIL:
				showToast("连接是失败");
				break;
			case XMPPHelper.LOGIN_SUCCESS:
				showToast("登录成功");
				// 保存帐号密码
				UserInfo.getInstance(Login.this).save(
						ETUtil.getText(etAccount), ETUtil.getText(etPassword));
				Login.this.setResult(RESULT_OK);
				Login.this.finish();
				break;
			case XMPPHelper.LOGIN_FAIL:
				showToast("登录失败");
				break;
			case XMPPHelper.REG_FAIL_409:
				showToast("注册失败,错误409");
				break;
			case XMPPHelper.REG_FAIL_OTHER:
				showToast("注册失败,其他错误");
				break;
			case XMPPHelper.REG_NO_RESPONSE:
				showToast("注册失败,服务器没有响应");
				break;
			case XMPPHelper.REG_SUCCESS:
				showToast("注册成功");
				// 保存帐号密码
				UserInfo.getInstance(Login.this).save(
						ETUtil.getText(etAccount), ETUtil.getText(etPassword));
				Login.this.setResult(RESULT_OK);
				Login.this.finish();
				break;
			default:
				break;
			}
		};
	};
}
