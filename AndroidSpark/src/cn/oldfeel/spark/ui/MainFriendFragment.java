package cn.oldfeel.spark.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import cn.oldfeel.spark.R;
import cn.oldfeel.spark.base.BaseFragment;
import cn.oldfeel.spark.fragment.FriendList;

/**
 * 联系人
 * 
 * @author oldfeel
 * 
 */
public class MainFriendFragment extends BaseFragment implements OnClickListener {
	/** 添加好友 */
	private static final int ADD_FRIEND = 0;
	private EditText etSearch;
	private Button btnAdd;

	public static MainFriendFragment newInstance() {
		MainFriendFragment fragment = new MainFriendFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.main_friend_view, null);
		etSearch = (EditText) view.findViewById(R.id.main_friend_search);
		btnAdd = (Button) view.findViewById(R.id.main_friend_add);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getActivity().getSupportFragmentManager().beginTransaction()
				.replace(R.id.main_friend_list, new FriendList()).commit();
		btnAdd.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(getActivity(), AddFriend.class);
		startActivityForResult(intent, ADD_FRIEND);
	}

	/**
	 * 更新好友列表
	 */
	public void updateFriendList() {
		getActivity().getSupportFragmentManager().beginTransaction()
				.replace(R.id.main_friend_list, new FriendList()).commit();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ADD_FRIEND) {
			updateFriendList();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}
