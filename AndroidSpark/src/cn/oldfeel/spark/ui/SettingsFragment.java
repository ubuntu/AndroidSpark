package cn.oldfeel.spark.ui;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import cn.oldfeel.spark.R;
import cn.oldfeel.spark.base.BaseFragment;
import cn.oldfeel.spark.util.DialogUtil;
import cn.oldfeel.spark.util.XMPPHelper;

/**
 * 设置界面
 * 
 * @author oldfeel
 * 
 */
@SuppressLint("HandlerLeak")
public class SettingsFragment extends BaseFragment implements OnClickListener {
	private Button btnChangePassword;
	private Button btnSetStatus;
	private Button btnCancelAccount;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.settings_view, null);
		btnChangePassword = (Button) view
				.findViewById(R.id.settings_change_password);
		btnSetStatus = (Button) view.findViewById(R.id.settings_set_status);
		btnCancelAccount = (Button) view
				.findViewById(R.id.settings_cancel_account);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		btnChangePassword.setOnClickListener(this);
		btnSetStatus.setOnClickListener(this);
		btnCancelAccount.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.settings_change_password:
			changePassword();
			break;
		case R.id.settings_cancel_account:
			cancel();
			break;
		case R.id.settings_set_status:
			setStatus();
			break;
		default:
			break;
		}
	}

	/**
	 * 设置状态
	 */
	private void setStatus() {
		new AlertDialog.Builder(getActivity()).setSingleChoiceItems(
				R.array.array_status, 0, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						XMPPHelper.getInstance().setStatus(handler, which);
						dialog.cancel();
					}
				}).show();
	}

	/**
	 * 删除帐号
	 */
	private void cancel() {
		XMPPHelper.getInstance().disConnect();
		Intent intent = new Intent(getActivity(), Login.class);
		startActivity(intent);
	}

	/**
	 * 修改密码
	 */
	private void changePassword() {
		Intent intent = new Intent(getActivity(), ChangePassword.class);
		startActivity(intent);
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case XMPPHelper.DELETE_FAIL:
				showToast("删除失败");
				break;
			case XMPPHelper.DELETE_SUCCESS:
				showToast("删除成功");
				break;
			case XMPPHelper.STATUS_AVAILABLE:
				showToast("在线");
				break;
			case XMPPHelper.STATUS_AWAY:
				showToast("离开");
				break;
			case XMPPHelper.STATUS_CHAT:
				showToast("Q我吧");
				break;
			case XMPPHelper.STATUS_DND:
				showToast("忙碌");
				break;
			case XMPPHelper.STATUS_NOT_AVAILABLE:
				showToast("隐身");
				break;
			case XMPPHelper.STATUS_UNAVAILABLE:
				showToast("离线");
				break;
			default:
				break;
			}
		};
	};

	/**
	 * 显示一个toast提示
	 * 
	 * @param msg
	 */
	private void showToast(String msg) {
		DialogUtil.getInstance().showToast(getActivity(), msg);
	}
}
