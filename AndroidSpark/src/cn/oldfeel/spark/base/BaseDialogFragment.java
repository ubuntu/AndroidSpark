package cn.oldfeel.spark.base;

import android.app.DialogFragment;
import android.os.Bundle;
import cn.oldfeel.spark.R;

/**
 * 如果一个界面是最终界面,即不会再跳转到其他activity时,建议使用此fragment
 * 
 * @author oldfeel
 * 
 */
public class BaseDialogFragment extends DialogFragment {
	/** 创建一个带标题的fragment */
	public static BaseDialogFragment newInstance(String title) {
		BaseDialogFragment fragment = new BaseDialogFragment();
		Bundle bundle = new Bundle();
		bundle.putString("title", title);
		fragment.setArguments(bundle);
		return fragment;
	};

	/**
	 * 设置默认style
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_TITLE,
				android.R.style.Theme_Holo_Light);
	}

	@Override
	public void onActivityCreated(Bundle arg0) {
		super.onActivityCreated(arg0);
		// 设置标题
		getDialog().setTitle(getArguments().getString("title"));
		// 设置dialog弹出/退出时的动画
		getDialog().getWindow().getAttributes().windowAnimations = R.style.anim_activity;
	}

}
