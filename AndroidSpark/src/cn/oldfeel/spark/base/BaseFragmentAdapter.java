package cn.oldfeel.spark.base;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * 可以手动保存fragment state 的适配器。
 * 
 * @author oldfeel
 * 
 */
public class BaseFragmentAdapter extends FragmentStatePagerAdapter {
	ArrayList<Fragment> list = new ArrayList<Fragment>();

	public BaseFragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	public void add(Fragment fragment) {
		list.add(fragment);
		notifyDataSetChanged();
	}

	@Override
	public Fragment getItem(int position) {
		return list.get(position);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return getItem(position).getArguments().getString("title");
	}
}
