package cn.oldfeel.spark.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import cn.oldfeel.spark.R;
import cn.oldfeel.spark.base.BaseBaseAdapter;
import cn.oldfeel.spark.item.HistoryItem;

/**
 * 历史记录适配器
 * 
 * @author oldfeel
 * @create Date:2013年11月6日下午2:27:08
 */
public class HistoryAdapter extends BaseBaseAdapter {

	public HistoryAdapter(Context context) {
		super(context, HistoryItem.class);
	}

	@Override
	public View getExView(int position, View convertView, ViewGroup parent) {
		HistoryItem item = (HistoryItem) getItem(position);
		convertView = inflater.inflate(R.layout.history_item, null);
		ImageView ivHead = (ImageView) convertView
				.findViewById(R.id.history_item_header);
		TextView tvFrom = (TextView) convertView
				.findViewById(R.id.history_item_name);
		TextView tvBody = (TextView) convertView
				.findViewById(R.id.history_item_body);
		TextView tvTime = (TextView) convertView
				.findViewById(R.id.history_item_time);
		ivHead.setBackgroundResource(R.drawable.ic_launcher);
		tvFrom.setText(item.from + "(" + item.count + "未读)");
		tvBody.setText(item.body);
		tvTime.setText(item.time);
		return convertView;
	}

	public void add(ArrayList<HistoryItem> list) {
		for (int i = 0; i < list.size(); i++) {
			addItem(list.get(i));
		}
		notifyDataSetChanged();
	}

}
