package cn.oldfeel.spark.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import cn.oldfeel.spark.R;
import cn.oldfeel.spark.base.BaseBaseAdapter;
import cn.oldfeel.spark.item.MsgItem;
import cn.oldfeel.spark.ui.ChatActivity;
import cn.oldfeel.spark.util.MediaUtil;

public class ChatAdapter extends BaseBaseAdapter {

	public ChatAdapter(Context context) {
		super(context, MsgItem.class);
	}

	@Override
	public View getExView(int position, View convertView, ViewGroup parent) {
		final MsgItem msg = (MsgItem) getItem(position);
		if (msg.getFrom().equals("IN")) {
			convertView = this.inflater.inflate(R.layout.formclient_chat_in,
					null);
		} else {
			convertView = this.inflater.inflate(R.layout.formclient_chat_out,
					null);
		}

		TextView useridView = (TextView) convertView
				.findViewById(R.id.formclient_row_userid);
		TextView dateView = (TextView) convertView
				.findViewById(R.id.formclient_row_date);
		TextView msgView = (TextView) convertView
				.findViewById(R.id.formclient_row_msg);
		useridView.setText(msg.getUserid());
		dateView.setText(msg.getDate());
		msgView.setText(msg.getMsg());

		if (!MsgItem.TYPE[2].equals(msg.getType())) {// normal
														// 普通msg
			TextView msgStatus = (TextView) convertView
					.findViewById(R.id.msg_status);
			msgStatus.setText(msg.getReceive() + "");
			final String path = ChatActivity.RECORD_ROOT_PATH
					+ msg.getFileName();
			Log.d("example", path);
			convertView.setOnClickListener(new OnClickListener() {// 点击查看
						@Override
						public void onClick(View v) {
							MediaUtil.ring(path);
						}
					});
		} else {
			TextView msgStatus = (TextView) convertView
					.findViewById(R.id.msg_status);
			msgStatus.setVisibility(View.GONE);// 隐藏
		}
		return convertView;
	}

}
