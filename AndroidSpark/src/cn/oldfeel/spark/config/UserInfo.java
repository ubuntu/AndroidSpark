package cn.oldfeel.spark.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * 用户信息
 * 
 * @author oldfeel
 * 
 */
public class UserInfo {
	static UserInfo userInfo;
	static SharedPreferences sp;
	static Editor editor;

	public static UserInfo getInstance(Context context) {
		if (userInfo == null) {
			userInfo = new UserInfo();
		}
		if (sp == null) {
			sp = context.getSharedPreferences("aspark", Context.MODE_PRIVATE);
		}
		if (editor == null) {
			editor = sp.edit();
		}
		return userInfo;
	}

	/**
	 * 保存帐号/密码
	 * 
	 * @param userName
	 * @param password
	 */
	public void save(String userName, String password) {
		editor.putString("userName", userName);
		editor.putString("password", password);
		editor.commit();
	}

	/**
	 * 获取用户名
	 * 
	 * @return
	 */
	public String getName() {
		return sp.getString("userName", "");
	}

	/**
	 * 获取密码
	 * 
	 * @return
	 */
	public String getPassword() {
		return sp.getString("password", "");
	}
}
