package cn.oldfeel.spark.fragment;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import cn.oldfeel.spark.R;
import cn.oldfeel.spark.adapter.HistoryAdapter;
import cn.oldfeel.spark.base.BaseFragment;
import cn.oldfeel.spark.db.ChatDbHelper;
import cn.oldfeel.spark.item.HistoryItem;
import cn.oldfeel.spark.ui.Main;

/**
 * 聊天记录列表
 * 
 * @author oldfeel
 * @create Date:2013年11月6日下午2:25:14
 */
public class HistoryList extends BaseFragment implements OnItemClickListener {
	ListView listView;
	HistoryAdapter adapter;

	public static HistoryList newInstance() {
		HistoryList fragment = new HistoryList();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.history_list, null);
		listView = (ListView) view.findViewById(R.id.history_list);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		adapter = new HistoryAdapter(getActivity());
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
		ArrayList<HistoryItem> list = ChatDbHelper.getInstance(getActivity())
				.getHistoryList();
		adapter.add(list);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		HistoryItem item = (HistoryItem) adapter.getItem(position);
		((Main) getActivity()).startChat(item.from);
	}
}
