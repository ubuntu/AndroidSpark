package cn.oldfeel.spark.fragment;

import org.jivesoftware.smack.packet.Message;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import cn.oldfeel.spark.R;
import cn.oldfeel.spark.adapter.ChatAdapter;
import cn.oldfeel.spark.base.BaseFragment;
import cn.oldfeel.spark.item.MsgItem;

/**
 * 聊天列表
 * 
 * @author oldfeel
 * @create Date:2013年11月6日上午10:22:00
 */
public class ChatList extends BaseFragment {
	ListView listView;
	ChatAdapter adapter;
	Handler handler = new Handler();

	public static ChatList newInstance() {
		ChatList fragment = new ChatList();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.chat_list, null);
		listView = (ListView) view.findViewById(R.id.chat_list);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		adapter = new ChatAdapter(getActivity());
		listView.setAdapter(adapter);
	}

	/**
	 * 添加信息
	 * 
	 * @param message
	 */
	public void add(Message message, String name) {
		name = name.substring(0, name.indexOf("@"));
		if (message.getFrom().contains(name)) {
			MsgItem msgItem = MsgItem.analyseMsgBody(message.getBody());
			if (msgItem != null) {
				add(msgItem);
			}
		}
	}

	public void add(MsgItem msg) {
		adapter.addItem(msg);
	}

	public void notifyDataSetChanged() {
		adapter.notifyDataSetChanged();
	}
}
